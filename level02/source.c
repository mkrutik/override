#include <stdio.h>
#include <string.h>
#include <stdint.h>

int  main(int  argc, char **argv)
{
    char    **var_120h;
    int64_t var_114h;
    char    filename[112]; // 0x110:  0x114 - 0x110
    char    ptr[48];    // 0xA0 :  0x110 - 0xA0
    char    format[100];  // 0x70 :  0xA0  - 0x70
    size_t  var_ch;
    FILE    *stream;

    memset(format, 0, 0xc);
    memset(ptr, 0, 5);
    memset(filename, 0, 0xc);

    stream = fopen("/home/users/level03/.pass", (char*)0x400bb0); // "r"
    if (stream == 0)
    {
        fwrite("ERROR: failed to open password file\n", 0x24, 1, stderr);
        exit(1);
    }

    var_ch = fread(ptr, 0x29, 1, stream);
    *(ptr + strcspn(ptr, (char*)0x400bf5)) = 0; // "\n"

    if (var_ch != 0x29)
    {
        fwrite("ERROR: failed to read password file\n", 0x24, 1, stderr);
        fwrite("ERROR: failed to read password file\n", 0x24, 1, stderr);
        exit(1);
    }
    fclose(stream);

    puts("===== [ Secure Access System v1.0 ] =====");
    puts("/***************************************\\");
    puts("| You must login to access this system. |");
    puts("\\**************************************/");

    printf("--[ Username: ");
    fgets(format, 0x64, stdin);
    format[strcspn(format, (char*)0x400bf5)] = 0; // "\n"

    printf("--[ Password: ");
    fgets(filename, 0x64, stdin);
    filename[strcspn(filename, 0x400bf5)] = 0; // "\n"
    puts("*****************************************");

    if (strncmp(ptr, filename, 0x29) == 0)
    {
        printf("Greetings, %s!\n", format);
        return system ("/bin/sh");
    }

    printf(format);
    puts(" does not have access!");
    exit(1);
}