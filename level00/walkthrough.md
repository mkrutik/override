After login we see next inforamtion.
```
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
Partial RELRO   No canary found   NX enabled    No PIE          No RPATH   No RUNPATH   /home/users/level00/level00
```

I investigated binary logic using `radar` tool. As we can see, there is `if/else` condition inside. If input value will be equal `5276` `system("/bin/sh")` function will be executed.

So let's exploit:
```
level00@OverRide:~$ ./level00
***********************************
* 	     -Level00 -		  *
***********************************
Password:5276

Authenticated!
$ cat /home/users/level01/.pass
uSq2ehEGT6c9S24zbshexZQBXUGrncxn5sD5QfGL
```

Flag: `uSq2ehEGT6c9S24zbshexZQBXUGrncxn5sD5QfGL`