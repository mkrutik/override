#include <stdio.h>

int main(void)
{
    int var_20h;
    int var_8h;

    puts("***********************************");
    puts("* \t     -Level00 -\t\t  *");
    puts("***********************************");

    printf("Password:");
    isoc99_scanf("%d", &var_8h);

    if (var_8h == 0x149c) // 0x149c == 5276
    {
        puts("\nAuthenticated!");
        system("/bin/sh");
        var_20h = 0;
    }
    else {
        puts("\nInvalid Password!");
        var_20h = 1;
    }
    return var_20h;
}
