#include <stdio.h>

char a_user_name[100];

int verify_user_pass(char * arg_8h)
{
    return asm_strcmp("admin", arg_8h);
}

int verify_user_name(void)
{
    puts("verifying username....\n");
    return asm_strcmp("dat_wil", a_user_name);
}

int main(void)
{
    int var_8h;
    int size;
    FILE *stream;
    char * s;
    int var_10h;

    memset(s, 0x10, 0);
    var_10h = 0;

    puts ("********* ADMIN LOGIN PROMPT *********");
    printf("Enter Username: ");

    stream = stdin;
    fgets(a_user_name, 0x100, stream);

    var_10h = verify_user_name();
    if (var_10h != 0)
    {
        puts("nope, incorrect username...\n");
    }
    else
    {
        puts("Enter Password: ");

        stream = stdin;
        fgets(s, 0x64, stream);

        var_10h = verify_user_pass(s);
        if (var_10h != 0)
        {
            if (var_10h == 0)
            {
                return 0;
            }
        }
        puts ("nope, incorrect password...\n");
    }
    return 1;
}
